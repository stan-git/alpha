#!/usr/bin/env python3 
"""Program for manipulating the memory of a process with the MemoryEditor module"""

__author__ = "S.Janik"
__license__ = "GNU GPLv3"
__version__ = "1.0.0"
__maintainer__ = "S.Janik"
__status__ = "Production"

import sys
import time
from threading import Thread
import curses
from MemoryEditor import MemoryEditor
from JsonFileHandler import *


# List of the menu entries
MENU = ['Set name of process', 'Search value in memory', 'Write to address',
        'Read from address', 'Show saved addresses', 'Exit']

_MEM = None         # MemoryEditor object
THREAD_RUN = True   # True if Thread for locking value should run
THREAD = None       # Thread object


def print_menu(stdscr, selected_row_idx, menu):
    """
    Function for printing menus

    :param stdscr: standard screen ; screen Object from curses
    :param selected_row_idx: row index of the current selected row
    :param menu: list with menu entries
    """
    h, w = stdscr.getmaxyx()

    for idx, row in enumerate(menu):
        x = (w - len(row)) // 2
        y = (h - len(menu)) // 2 + idx
        if idx == selected_row_idx:
            stdscr.attron(curses.color_pair(1))
            stdscr.addstr(y, x, str(row))
            stdscr.attroff(curses.color_pair(1))
        else:
            stdscr.addstr(y, x, str(row))

    stdscr.refresh()


def get_in(stdscr, msg):
    """
    Function for getting input with curses

    :param stdscr: standard screen ; screen Object from curses
    :param msg: message that shall being printed
    :return: text that was entered
    """
    curses.echo()
    curses.curs_set(1)

    print_msg(stdscr, msg)

    text = stdscr.getstr()
    curses.curs_set(0)
    curses.noecho()

    return text


def print_msg(stdscr, msg):
    """
    Function for printing text with curses

    :param stdscr: standard screen ; screen Object from curses
    :param msg: message that shall being printed
    """
    stdscr.addstr(msg)
    stdscr.refresh()


def print_err(stdscr, msg):
    """
        Function for printing text with curses

        :param stdscr: standard screen ; screen Object from curses
        :param msg: error that shall being printed
        """
    print_menu(stdscr, 1, [msg, "OK"])
    get_in(stdscr, "")
    stdscr.clear()


def _process_name_isset():
    """
    Function to check if the process name is set.
    Otherwise we couldn't set the pid. That's bad.
    """
    return not (_MEM is None)


def _menu_read(stdscr):
    addr = int(get_in(stdscr, "Adresse in HEX\n"
                              ">> "), 16)

    r = _MEM.read_addr(addr)
    if not r:
        print_err(stdscr, "[!] Invalid address.")
        return

    print_err(stdscr," {} : {}".format(int.from_bytes(r, sys.byteorder), r))


def _menu_process_name(stdscr):
    process_name = get_in(stdscr, "Name of process:\n"
                                  ">> ")
    global _MEM
    _MEM = MemoryEditor(process_name)
    success, err = _MEM.init_mem()

    if success is not True:
        print_err(stdscr, err)
        _MEM = None


def _menu_search(stdscr):
    addresses = {}
    datatype = get_in(stdscr, "i = int | s = string: ")

    while True:
        val = get_in(stdscr, "Data to look for or \"!\" to finish\n"
                             ">> ")

        if val == b'!':
            break

        if datatype == b'i':
            val = int(val)

        addresses = _MEM.remove_by_value(addresses, val)

        if addresses is False:
            print_err(stdscr, "[!] You need to run this program as root")
            return

        print_msg(stdscr, "COUNT: {}  addresses".format(len(addresses.keys())))

    for addr in addresses:
        print_msg(stdscr, addr)
        print_msg(stdscr, str(addresses[addr]))
        print_msg(stdscr, "\n")

    val = get_in(stdscr, "Want to save the addresses for later ? (y/n)\n"
                         ">> ")
    if val == b'y':
        save_json(addresses)


def _menu_write(stdscr):
    datatype = get_in(stdscr, "i = int | s = string: ")
    val = get_in(stdscr, "Data you want to write\n"
                         ">> ")
    if datatype == b'i':
        val = int(val)

    addr = int(get_in(stdscr, "Adresse in HEX\n"
                              ">> "), 16)

    success = _MEM.write_addr(val, addr)
    if not success:
        print_err(stdscr, "[!] Couldn't find the process anymore")


def _get_submenu_list():
    data = load_json()
    submenu = []
    submenu_string = ""
    for addr in data:
        submenu_string += "{}: ".format(addr)
        for key, val in data[addr].items():
            submenu_string += "{} : {} ".format(key, val)

        submenu.append(submenu_string)
        submenu_string = ""
    return submenu


def _lock_value(addresses):
    while THREAD_RUN:
        for addr in addresses:
            success = _MEM.write_addr(addresses[addr], addr)
            if not success:
                return False

        time.sleep(1)


def _menu_lock(stdscr):

    current_row_idx = 0
    addresses_lock = {}
    global THREAD_RUN, THREAD

    while 1:
        addresses = load_json()
        submenu = _get_submenu_list()
        submenu_lock = submenu.__add__(["Update", "Start", "Stop", "Remove", "Back"])

        print_menu(stdscr, current_row_idx, submenu_lock)

        key = stdscr.getch()

        stdscr.clear()

        if key == curses.KEY_UP and current_row_idx > 0:
            current_row_idx -= 1
        elif key == curses.KEY_DOWN and current_row_idx < len(submenu_lock) - 1:
            current_row_idx += 1
        elif key == curses.KEY_ENTER or key in [10, 13]:

            if current_row_idx == len(submenu_lock) - 1:
                break

            elif current_row_idx < len(submenu_lock) - 5:
                addr = list(addresses.keys())[current_row_idx]
                addresses_lock[int(addr, 16)] = addresses[addr]["data"]
                print_err(stdscr, "{} selected.".format(addr))

            elif current_row_idx == submenu_lock.index("Update") and len(addresses_lock) > 0:
                for addr in addresses:
                    addr = int(addr,16)
                    data = _MEM.read_addr(addr)

                    if not data:
                        print_err(stdscr,"[!] Seems like this address is deprecated")
                        break
                    
                    if type(addresses[hex(addr)]["data"]) == int:
                        addresses[hex(addr)]["data"] = int.from_bytes(data, sys.byteorder)
                    else:
                        addresses[hex(addr)]["data"] = str(data)

                save_json(addresses)
                _menu_lock(stdscr)

            elif current_row_idx == submenu_lock.index("Start") and len(addresses_lock) > 0:
                THREAD_RUN = True
                THREAD = Thread(target=_lock_value, args=(addresses_lock,))
                THREAD.start()

            elif current_row_idx == submenu_lock.index("Stop") and len(addresses_lock) > 0:
                THREAD_RUN = False
                THREAD.join()

            elif current_row_idx == submenu_lock.index("Remove") and len(addresses_lock) > 0:
                for key in addresses_lock:
                    del addresses[hex(key)]

                write_json(addresses)


_menu_actions = {
    0: _menu_process_name,
    1: _menu_search,
    2: _menu_write,
    3: _menu_read,
    4: _menu_lock,
}


def main(stdscr):
    curses.curs_set(0)
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_WHITE)
    current_row_idx = 0

    while 1:
        print_menu(stdscr, current_row_idx, MENU)

        key = stdscr.getch()

        stdscr.clear()

        if key == curses.KEY_UP and current_row_idx > 0:
            current_row_idx -= 1
        elif key == curses.KEY_DOWN and current_row_idx < len(MENU) - 1:
            current_row_idx += 1
        elif key == curses.KEY_ENTER or key in [10, 13] \
                and (_process_name_isset() or current_row_idx in [0, len(MENU) - 1]):

            if current_row_idx == len(MENU) - 1:
                break

            if current_row_idx in _menu_actions.keys():
                _menu_actions[current_row_idx](stdscr)

            stdscr.clear()


if __name__ == "__main__":
    curses.wrapper(main)

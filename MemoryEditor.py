#!/usr/bin/env python

""" Module to manipulate the memory of a specific process """

import re
import struct
from subprocess import check_output, CalledProcessError

__author__ = "S.Janik"
__license__ = "GNU GPLv3"
__version__ = "1.0.0"
__maintainer__ = "S.Janik"
__status__ = "Production"


class MemoryEditor:
    def __init__(self, process_name):
        self.process = {"name": process_name, "pid": "", "fmaps": "", "fmem": ""}
        self.needle = ""
        self.regions = []

    def init_mem(self):
        """
        Set some variables

        set the pid, paths to the process files "mem" and "maps"
        and get the memory regions ([heap], [stack] and so on..)

        :return: False and Error  - if there was something wrong
        """
        success = self._set_pid()
        if not success:
            return False, "[!] Can't find the process"

        success = self._set_proc_file()
        if not success:
            return False, "[!] You need to run this program as root"

        self._set_memory_regions()

        return True, ""

    def get_pid(self):
        """:returns the pid of the observed process"""
        return self.process["pid"]

    def _set_pid(self):
        """
        Set the pid

        use the "pidof" command and get the result

        :returns
            False - if Error
            True  - on success
        """
        try:
            self.process["pid"] = check_output(["pidof", self.process["name"]])
            self.process["pid"] = self.process["pid"].decode().rstrip()
        except CalledProcessError:
            return False

        return True

    def _set_proc_file(self):
        """
        Generating the "mem" and "maps" file path

        :return
            False - if PID is not set
            True - on success
        """
        if not self.process["pid"]:
            return False

        self.process["fmaps"] = "/proc/{}/maps".format(self.process["pid"])
        self.process["fmem"] = "/proc/{}/mem".format(self.process["pid"])

        return True

    def _pack_bin(self, var):
        """
        transform var to bytes and set self.needle

        :param var: variable to transform
        """
        if type(var) == int:
            self.needle = struct.pack("i", var)
        elif type(var) == bytes:
            self.needle = var
        else:
            self.needle = bytes(var, "ASCII")

    def remove_by_value(self, old_addresses, val):
        """
        Function to select a specific address by changing the value
        and check which addresses of old_addresses changed

        :param old_addresses: original dict with memory addresses
        :param val: new value expected in the memory address
        :return:
            new_addresses (dict of addresses) - if old_addresses is empty
            new_dict - dict with addresses which changed to the new value
        """
        new_addresses = self.lookup_value(val)

        if not old_addresses:
            return new_addresses

        return {key: new_addresses[key] for key in old_addresses.keys() if key in new_addresses.keys()}

    def _set_memory_regions(self):
        """
        Function to read all memory regions of a process

        """
        regions = []

        with open(self.process["fmaps"], "r") as map_file:
            for line in map_file:
                match = re.match(r'([0-9A-Fa-f]+)-([0-9A-Fa-f]+) ([-r])', line)
                if match.group(3) == 'r' and "deleted" not in line:  # if this is a readable region
                    r = line.split(" ")  # r = region
                    r = r[len(r) - 1].rstrip()

                    start = match.group(1)
                    end = match.group(2)
                    if r:
                        regions.append({"region": r, "start": int(start, 16), "end": int(end, 16)})
        self.regions = regions

    def lookup_value(self, searched):
        """
        Function to search given data in the process memory

        :param searched: given data to look for
        :return:
            False - on Error
            address_info - dict with information about addresses with the
                           searched value
        """
        self._pack_bin(searched)
        address_info = {}

        try:
            with open(self.process["fmem"], "rb") as mem_file:
                for r in self.regions:
                    mem_file.seek(r["start"])

                    try:
                        haystack = mem_file.read(r["end"] - r["start"])
                    except OSError:
                        continue

                    data = re.finditer(self.needle, haystack)
                    for addr in data:
                        address_info[hex(r["start"] + addr.start())] = {"region": r["region"],
                                                                        "offset": addr.start(),
                                                                        "data": searched}
        except PermissionError:
            return False
        return address_info

    def read_addr(self, addr):
        """
        Function to read from a given memory address

        :param addr: Address to read from
        :return:
            False - on Error
            1 Byte of data - on Success
        """
        try:
            with open(self.process["fmem"], "rb") as mem_file:
                mem_file.seek(addr)
                return mem_file.read(1)

        except PermissionError:
            return False
        except OSError:
            return False

    def write_addr(self, val, addr):
        """
        Function to write to a given memory address

        :param val: value to write to the addresses
        :param addr: memory address where to write val
        :return:
            False - on Error
            True - on Success
        """
        self._pack_bin(val)
        try:
            with open(self.process["fmem"], "wb") as mem_file:
                mem_file.seek(addr)
                mem_file.write(self.needle)

            return True
        except FileNotFoundError:
            return False
        except OSError:
            return False

"""
functions for handling files

save addresses
load addresses

"""
import json


def save_json(addr_dict):
    data = load_json()
    for key in addr_dict.keys():
        data[key] = addr_dict[key]

    with open("./addr.json", "w+") as f:
        f.write(json.dumps(data, indent=2))


def write_json(data):
    with open("./addr.json", "w") as f:
        f.write(json.dumps(data, indent=2))


def reset_json():
    with open("./addr.json", "w+") as f:
        f.write(json.dumps("{}", indent=2))


def load_json():
    try:
        with open("addr.json", "r") as f:
            data = json.load(f)
    except FileNotFoundError:
        return {}
    except json.JSONDecodeError:
        return {}

    return data
